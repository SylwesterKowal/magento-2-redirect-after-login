<?php
/**
 * Copyright © kowal sp. z o.o. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\RedirectAfterLogin\Model\Config\Source;

class Groups implements \Magento\Framework\Option\ArrayInterface
{

    public function __construct(
        \Magento\Customer\Model\ResourceModel\Group\Collection $customerGroup
    ) {
        $this->_customerGroup = $customerGroup;
    }

    public function toOptionArray()
    {
        return $this->getCustomerGroups();
    }

    public function toArray()
    {
        return ['' => __('')];
    }

    public function getCustomerGroups() {
        $customerGroups = $this->_customerGroup->toOptionArray();
        array_unshift($customerGroups, array('value'=>'', 'label'=>'Any'));
        return $customerGroups;
    }
}

