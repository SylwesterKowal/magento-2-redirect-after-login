<?php
/**
 * Copyright © kowal sp. z o.o. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\RedirectAfterLogin\Controller\Login;

use Exception;
use Magento\Checkout\Model\Cart;
use Magento\Customer\Model\Account\Redirect as AccountRedirect;
use Magento\Customer\Model\SessionFactory;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\Forward;
use Magento\Framework\Controller\Result\Redirect;
use Magento\Framework\Controller\ResultInterface;
use Kowal\RedirectAfterLogin\Model\LogFactory;

class Index extends \Magento\Framework\App\Action\Action
{

    /**
     * @var AccountRedirect
     */
    protected $accountRedirect;

    /**
     * @var SessionFactory
     */
    protected $session;

    /**
     * @var LogFactory
     */
    protected $_logFactory;

    /**
     * @var Cart
     */
    protected $checkoutCart;


    protected $resultPageFactory;

    /**
     * Index constructor.
     * @param Context $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Magento\Customer\Model\CustomerFactory $customerFactory
     * @param SessionFactory $customerSession
     * @param AccountRedirect $accountRedirect
     * @param Cart $checkoutCart
     * @param LogFactory $logFactory
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Customer\Model\SessionFactory $customerSession,
        AccountRedirect $accountRedirect,
        Cart $checkoutCart,
        LogFactory $logFactory
    )
    {
        $this->session = $customerSession;
        $this->customerFactory = $customerFactory;
        $this->accountRedirect = $accountRedirect;
        $this->checkoutCart = $checkoutCart;
        $this->_logFactory = $logFactory;

        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

    /**
     * Execute view action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $token = $this->getRequest()->getParam('key');
        $session = $this->session->create();

        $log = $this->_logFactory->create()->load($token, 'token');
        if (!$log || !$log->getId()) {
            return $this->_redirect('noRoute');
        }

        try {
            if ($session->isLoggedIn()) {
                $session->logout();
            } else {
                $this->checkoutCart->truncate()->save();
            }
        } catch (Exception $e) {
            $this->messageManager->addNoticeMessage(__('Cannot truncate cart items.'));
        }

        try {

            $customer = $this->customerFactory->create()->load($log->getCustomerId());

            $session->loginById($log->getCustomerId());
            $session->setCustomerAsLoggedIn($customer);
            $session->regenerateId();

//            $log->setIsLoggedIn(true)
//                ->save();


            $redirectUrl = $this->accountRedirect->getRedirectCookie();
            $this->accountRedirect->clearRedirectCookie();
            $resultRedirect = $this->resultRedirectFactory->create();
            $resultRedirect->setUrl($this->_redirect->success($redirectUrl));

            return $resultRedirect;

        } catch (Exception $e) {
            $this->messageManager->addError(
                __('An unspecified error occurred. Please contact us for assistance.')
            );
        }
        return $this->resultPageFactory->create();
    }
}