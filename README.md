# Mage2 Module Kowal RedirectAfterLogin

    ``kowal/module-redirectafterlogin``

 - [Main Functionalities](#markdown-header-main-functionalities)
 - [Installation](#markdown-header-installation)
 - [Configuration](#markdown-header-configuration)
 - [Specifications](#markdown-header-specifications)
 - [Attributes](#markdown-header-attributes)


## Main Functionalities
Przekierowanie klienta B2B na stronę dla klientów b2b

## Installation
\* = in production please use the `--keep-generated` option

### Type 1: Zip file

 - Unzip the zip file in `app/code/Kowal`
 - Enable the module by running `php bin/magento module:enable Kowal_RedirectAfterLogin`
 - Apply database updates by running `php bin/magento setup:upgrade`\*
 - Flush the cache by running `php bin/magento cache:flush`

### Type 2: Composer

 - Make the module available in a composer repository for example:
    - private repository `repo.magento.com`
    - public repository `packagist.org`
    - public github repository as vcs
 - Add the composer repository to the configuration by running `composer config repositories.repo.magento.com composer https://repo.magento.com/`
 - Install the module composer by running `composer require kowal/module-redirectafterlogin`
 - enable the module by running `php bin/magento module:enable Kowal_RedirectAfterLogin`
 - apply database updates by running `php bin/magento setup:upgrade`\*
 - Flush the cache by running `php bin/magento cache:flush`


## Configuration

 - Włącz moduł (redirect_after_login/settings/enable)

 - Przekierowanie na stronę (redirect_after_login/settings/website)

 - Grupy klientów (redirect_after_login/settings/groups)


## Specifications

 - Plugin
	- afterExecute - Magento\Customer\Controller\Account\LoginPost > Kowal\RedirectAfterLogin\Plugin\Frontend\Magento\Customer\Controller\Account\LoginPost


## Attributes



