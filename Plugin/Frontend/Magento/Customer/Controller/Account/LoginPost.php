<?php
/**
 * Copyright © kowal sp. z o.o. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\RedirectAfterLogin\Plugin\Frontend\Magento\Customer\Controller\Account;

use Magento\Framework\Url;

class LoginPost
{

    public function __construct(
        \Magento\Framework\Controller\ResultFactory $resultFactory,
        \Magento\Framework\App\Response\RedirectInterface $redirect,
        \Magento\Framework\Session\SessionManager $sessionManager,
        \Kowal\RedirectAfterLogin\Helper\Data $dataHelper,
        \Magento\Framework\App\RequestInterface $request,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
//        \Mageplaza\LoginAsCustomer\Model\LogFactory $logFactory,
        \Kowal\RedirectAfterLogin\Model\LogFactory $logFactory
    )
    {
        $this->resultFactory = $resultFactory;
        $this->redirect = $redirect;
        $this->sessionManager = $sessionManager;
        $this->dataHelper = $dataHelper;
        $this->_customerFactory = $customerFactory;
        $this->_logFactory = $logFactory;
    }

    public function afterExecute(
        \Magento\Customer\Controller\Account\LoginPost $subject,
        $result
    )
    {
        if ($this->dataHelper->getGeneralCfg('enable')) {
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $customerSession = $objectManager->create('Magento\Customer\Model\Session');
            if ($customerSession->isLoggedIn()) {
                $groupId = $customerSession->getCustomerGroupId();
                if ( $redirect_ = $this->redirectByGroup($groupId) ) {

                    $customerId = $customerSession->getId();
                    $customer = $this->_customerFactory->create()->load($customerId);
                    if (!$customer || !$customer->getId()) {
                        $this->messageManager->addErrorMessage(__('Customer does not exist.'));
                        return $this->_redirect('customer');
                    }

                    $token = $this->dataHelper->getLoginToken();

                    $log = $this->_logFactory->create();
                    $log->setData([
                        'token' => $token,
                        'customer_id' => $customer->getId()
                    ])->save();

                    $store = $this->dataHelper->getStore($customer);
                    $loginUrl = $objectManager->create(Url::class)
                        ->setScope($store)
                        ->getUrl('redirect/login/index', ['key' => $token, '_nosid' => true]);

                    $resultRedirect = $this->resultFactory->create(
                        \Magento\Framework\Controller\ResultFactory::TYPE_REDIRECT
                    );
                    $result = $resultRedirect->setUrl($loginUrl);
                }
            }
        }
        return $result;
    }

    private function redirectByGroup($groupId)
    {
        if ($groups = $this->dataHelper->getGeneralCfg('groups')) {
            $groups = explode(",",$groups);
            if (in_array($groupId, $groups)) {
                return true;
            }
        }

        return false;
    }
}

