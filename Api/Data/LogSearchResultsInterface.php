<?php
/**
 * Copyright © kowal sp. z o.o. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\RedirectAfterLogin\Api\Data;

interface LogSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{

    /**
     * Get Log list.
     * @return \Kowal\RedirectAfterLogin\Api\Data\LogInterface[]
     */
    public function getItems();

    /**
     * Set token list.
     * @param \Kowal\RedirectAfterLogin\Api\Data\LogInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}

