<?php
/**
 * Copyright © kowal sp. z o.o. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\RedirectAfterLogin\Api\Data;

interface LogInterface extends \Magento\Framework\Api\ExtensibleDataInterface
{

    const CUSTOMER_ID = 'customer_id';
    const LOG_ID = 'log_id';
    const TOKEN = 'token';

    /**
     * Get log_id
     * @return string|null
     */
    public function getLogId();

    /**
     * Set log_id
     * @param string $logId
     * @return \Kowal\RedirectAfterLogin\Api\Data\LogInterface
     */
    public function setLogId($logId);

    /**
     * Get token
     * @return string|null
     */
    public function getToken();

    /**
     * Set token
     * @param string $token
     * @return \Kowal\RedirectAfterLogin\Api\Data\LogInterface
     */
    public function setToken($token);

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Kowal\RedirectAfterLogin\Api\Data\LogExtensionInterface|null
     */
    public function getExtensionAttributes();

    /**
     * Set an extension attributes object.
     * @param \Kowal\RedirectAfterLogin\Api\Data\LogExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Kowal\RedirectAfterLogin\Api\Data\LogExtensionInterface $extensionAttributes
    );

    /**
     * Get customer_id
     * @return string|null
     */
    public function getCustomerId();

    /**
     * Set customer_id
     * @param string $customerId
     * @return \Kowal\RedirectAfterLogin\Api\Data\LogInterface
     */
    public function setCustomerId($customerId);
}

